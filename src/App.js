import React, { Component } from 'react'
import './App.css'
import person from "./person/person.png"
import Navbar from "./componentes/Navbar"
import Header from "./componentes/Header"
import Tarjetas from "./componentes/Tarjetas"
import Formulario from "./componentes/Formulario"
class App extends Component {
  render() {
    return (
      <div className="App">
      <Navbar/>
      
      <Header/>


        <section className="py-3"></section>        

        <section className="text-warning py-5" id="formato">
           <div className="container">
             <div className="row">
               <div className="col-md-4">
                 <input type="text" className="form-control form-control-lg"
                   placeholder="Enter your name"
                 />
               </div>
               <div className="col-md-4">
               <input type="email" className="form-control form-control-lg"
                   placeholder="Enter your email"/>
               </div>
               <div className="col-md-4">
                 <button className="btn btn-danger btn-lg btn-block">
                    Suscribe
                 </button>
                </div>
                
             </div>
           </div>
        </section>

        <Tarjetas/>

        <section className="py-5"></section>      

        <section>
          <div className="container text-center text-warning" id="why">
            <div className="row">
              <div className="m-5">
                <h3>Why this product?</h3>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatem aperiam optio aspernatur deleniti deserunt quod suscipit nostrum, dolore magnam quis.</p>
                </div>
            </div>
          </div>
        </section>

        <section className="py-3"></section> 

        <section className="container text-warning text-center p-5">
         <h1>FAQ</h1>
           <div className="row">
           <div className="accordion" id="accordionExample">
            <div className="card">
              <div className="card-header" id="headingOne">
                <h2 className="mb-0">
                  <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Question #1
                  </button>
                </h2>
              </div>
          
              <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div className="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header" id="headingTwo">
                <h2 className="mb-0">
                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Question #2
                  </button>
                </h2>
              </div>
              <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div className="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header" id="headingThree">
                <h2 className="mb-0">
                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Question #3
                  </button>
                </h2>
              </div>
              <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div className="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
          </div>
           </div>
        </section>
      
        <section className="py-3"></section>

        <section className="team text-center text-warning">
           <div className="container p-5">
             <h1>Team</h1>
             <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias, officiis!</p>
               <div className="row">
                <div className="col-lg-3">
                  <div className="tarjetas">
                    <div className="card-body">
                      <img src={person} alt="desarrollador" className="img-fluid rounded-circle w-50"/>
                       <h3 className="py-2">John Carter</h3>
                       <p>Lorem ipsum dolor sit amet.</p>
                        <div className="d-flex flex-row justify-content-center">  
                          <div className="p-4"> 
                             <a href="http://facebook.com/NobelesDeLiteratura" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-facebook"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-twitter"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-instagram"></i>
                             </a>
                          </div>

                        </div>
                    </div>
                  </div>
                 </div>
                 <div className="col-lg-3">
                  <div className="tarjetas">
                    <div className="card-body">
                      <img src={person} alt="desarrollador" className="img-fluid rounded-circle w-50"/>
                       <h3 className="py-2">John Carter</h3>
                       <p>Lorem ipsum dolor sit amet.</p>
                        <div className="d-flex flex-row justify-content-center">  
                          <div className="p-4"> 
                             <a href="http://facebook.com/NobelesDeLiteratura" target="_blank" rel="noopener noreferrer"> 
                              <i className="fab fa-facebook"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-twitter"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-instagram"></i>
                             </a>
                          </div>

                        </div>
                    </div>
                  </div>
                 </div>
 
                 <div className="col-lg-3">
                  <div className="tarjetas">
                    <div className="card-body">
                      <img src={person} alt="desarrollador" className="img-fluid rounded-circle w-50"/>
                       <h3 className="py-2">John Carter</h3>
                       <p>Lorem ipsum dolor sit amet.</p>
                        <div className="d-flex flex-row justify-content-center">  
                          <div className="p-4"> 
                             <a href="http://facebook.com/NobelesDeLiteratura" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-facebook"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-twitter"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-instagram"></i>
                             </a>
                          </div>

                        </div>
                    </div>
                  </div>
                 </div>
 
                 <div className="col-lg-3">
                  <div className="tarjetas">
                    <div className="card-body">
                      <img src={person} alt="desarrollador" className="img-fluid rounded-circle w-50"/>
                       <h3 className="py-2">John Carter</h3>
                       <p>Lorem ipsum dolor sit amet.</p>
                        <div className="d-flex flex-row justify-content-center">  
                          <div className="p-4"> 
                             <a href="http://facebook.com/NobelesDeLiteratura" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-facebook"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-twitter"></i>
                             </a>
                          </div>

                          <div className="p-4"> 
                             <a href="#" target="_blank" rel="noopener noreferrer">
                              <i className="fab fa-instagram"></i>
                             </a>
                          </div>

                        </div>
                    </div>
                  </div>
                 </div>
 
               </div>
               
            </div>
         

        </section>
       
        <section className="py-3"></section>

       <Formulario/>
        
        <footer>  
          <div className="container p-1 text-center">
           <p>Copyright &copy; 2018</p>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
