import React, { Component } from 'react';
import logo from "../logo/phoenix.png";

class Navbar extends Component {
    render () {
        return(
            <nav className="navbar navbar-expand-lg">
            <div className="container">
               <a className="navbar-brand" href="/">
                 <img src={logo} className="logo" alt="Logo"/>
                </a>
               <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                 <span className="navbar-toggler-icon"></span>
               </button>
               <div className="collapse navbar-collapse" id="navbarNav">
                 <ul className="navbar-nav ml-auto">
                   <li className="nav-item">
                     <a className="nav-link" href="#">About</a>
                   </li>
                   <li className="nav-item">
                     <a className="nav-link" href="#">Team</a>
                   </li>
                   <li className="nav-item">
                     <a className="nav-link" href="#">Contact</a>
                   </li>
                 </ul>
                </div>
            </div>
         </nav>
        )
    }
}

export default Navbar;