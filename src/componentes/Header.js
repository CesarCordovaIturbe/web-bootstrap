import React, { Component } from 'react';
import product from "../product/collar.jpg";

class Header extends Component {
    render () {
        return(
            <header className="main-header">
            <div className="background-overlay text-warning py-5">
              <div className="container">
                <div className="row">
                  <div className="col-md-6 text-center justify-content-center align-self-center">
                     <h1 className="title">This is title or phrase related to the product</h1>
                      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, voluptates.</p>
                      <a href="#" className="btn btn-danger btn-lg">Read more</a>
                  </div>
                  <div className="col-md-6">
                    <img src={product} id="producto" className="img-fluid d-none d-sm-block" alt="product"/>
                  </div>
                </div>
              </div>
            </div>
          </header>          
            )
        }
    }

export default Header;

