import React, { Component } from 'react';
import logo from "../logo/phoenix.png";

 class Formulario extends Component {
     constructor (props) {
         super(props)
         this.state={name:"",email:"",comentario:""}
      }
      onChangeName = (e) => {this.setState({name:e.target.value})}
     render () {
          const {name,email,comentario} = this.state
         return(
            <section className="py-3 text-warning" id="formulario">
              <div className="container">
                <div className="row">
                  <div className="col-lg-9">
                    <h3>Contact</h3>
                    <p>Lorem ipsum dolor sit amet.</p>  
                    <form action="">
                      <div className="input-group mb-3">
                         <div className="input-group-prepend">
                            <i className="fas fa-user input-group-text bg-warning"></i>
                         </div>
                         <input type="text" className="form-control"
                           placeholder="Name" value={name} onChange={this.onChangeName}
                         />
                      </div>
                      <div className="input-group mb-3">
                         <div className="input-group-prepend">
                            <i className="fas fa-at input-group-text bg-warning"></i>
                         </div>
                         <input type="email" className="form-control" value={email}
                           placeholder="Email"
                         />
                      </div>
                      <div className="input-group mb-3">
                         <div className="input-group-prepend">
                            <i className="fas fa-edit input-group-text bg-warning"></i>
                         </div>
                         <textarea name="" id="" cols="30" rows="7" placeholder="Write a message" value={comentario} className="form-control"></textarea>
                      </div>
                      <button className="btn btn-danger btn-lg btn-block">
                        Send Your Message
                     </button>
                    </form>
                  </div>   
                  <div className="col-lg-3 align-self-center"> 
                    <img src={logo} alt=""/>
                  </div>             
    
                </div>
              </div>
    
            </section>
         )
     }
 }

 export default Formulario;
