import React, { Component } from 'react';
import { map } from 'lodash';

const tarjetasData = [ {name:"Feature One", description:"Lorem ipsum, dolor sisdfsfsfdsft amet consectetur adipisicing."},
                       {name:"Feature Dos", description:"Lorem ipsum, dolorsdfsfsf s sit amet consectetur adipisicing."},
                       {name:"Feature Three", description:"Lorem ipsum, dolor sit amsddf dgdfsfs dsf set consectetur adipisicing."},
                       {name:"Feature Four", description:"Lorem dfgdf ipsum, dolor sit amet c dfgdfg dg d nsectetur adipisicing."} ]

class Tarjeta extends Component {
    render() {
       const{name,description}=this.props
        return (
            <div className="col-md-3">
            <div className="card text-center border-warning">
              <div className="card-body">
                <h3>{name}</h3>
                <p>{description}</p>
              </div>
            </div>
          </div>
        )
    }
}

class Tarjetas extends Component {
    render () {
        const lista = map (
            tarjetasData, (tarjeta, i) => { return <Tarjeta name={tarjeta.name} description={tarjeta.description}/> }
        )
        return (
            <section className="py-5" id="tarjetas"> 
            <div className="container">
              <div className="row">
               {lista}              
               </div>
              </div>
           </section>
        )
    }
}

export default Tarjetas;